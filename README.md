# XTM

#ParserPSQL
Project created to parse Redmine DB PSQL output (Expanded display on) using mustache to html(can use other extensions).  
pip install pystache  
  
1) Prepare input files in input fir  
2) Prepare layout - according to mustache documentation (https://mustache.github.io/)  
    sample attached  
3) Provide directories and layout in argument:  
example:  
    python3 main.py email_layout.mustache in out  
