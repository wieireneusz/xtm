import parser as p
import sys

def main():
    parser= p.Parser()
    try:
        parser.setTemplate(open(sys.argv[1], 'r').read())
        parser.setInputDirectory(sys.argv[2])
        parser.setOutputDirectory(sys.argv[3])
    except FileNotFoundError as e:
        print(e)
    except Exception as e:
        print(e)
    else:
        parser.findAllInputFiles()
        for file in parser.files:
            filesPwd= list(open(parser.inputDirectoryPath+'/'+file,'r'))
            jDic= parser.parsePsqlToDictionaries(list(filesPwd))
            rendered= parser.render(jDic)
            jsonFile= open(parser.outputDirectoryPath+'/'+file+'.json','w')
            jsonFile.write(jDic)
            jsonFile.close()
            htmlFile= open(parser.outputDirectoryPath+'/'+file+'.html','w')
            htmlFile.write(rendered)
            htmlFile.close()


if __name__ == '__main__':
    main()
