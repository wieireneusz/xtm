import re
import json
import pystache
import os
import collections
# -*- coding: UTF-8 -*-


class Parser:
    def __init__(self):
        self.files= []
        self.template= 'NO TEMPLATE CONFIGURED'
        self.inputDirectoryPath= os.path.dirname(os.path.realpath(__file__))+'/in'
        self.outputDirectoryPath= os.path.dirname(os.path.realpath(__file__))+'/out'   

    def setTemplate(self, templatePath):
        self.template= templatePath

    def setInputDirectory(self, inputDirectoryPath):
        self.inputDirectoryPath= inputDirectoryPath        
        
    def setOutputDirectory(self, outputDirectoryPath):
        self.outputDirectoryPath= outputDirectoryPath
    
    #checking input directory for input files
    def findAllInputFiles(self):
        for file in os.listdir(self.inputDirectoryPath):
            if not file.startswith('.'):
                self.files.append(file)
    
    #parsing standard psql output to json dictionary
    def parsePsqlToDictionaries(self, inputFile):
        listOfDictionaries= []
        for i in range(0,len(inputFile)-1):
            compiled= re.match(r'-\[ [^\d]+(\d+) \]-*', inputFile[i])
            if compiled != None:
                rowId= int(compiled.groups()[0])
                listOfDictionaries.append({})
                index= len(listOfDictionaries)-1
                listOfDictionaries[index]= {'order_id':rowId}
            else:
                compiled= list(re.match(r'^(\S*)\s*\| (.*)', inputFile[i]).groups())
                compiled[1]= re.sub(r'\\r', '', compiled[1])
                if re.match(r'^\s*$', compiled[0]):
                    listOfDictionaries[index][elem]+= '<br>' + compiled[1]
                else:
                    elem= compiled[0]
                    listOfDictionaries[index][compiled[0]]= compiled[1]
        dic= {'tickets': listOfDictionaries}
        jDic= json.dumps(dic, indent=4)
        return jDic

    #rendering template using pystache
    def render(self, jDic, template= None):
        if template == None:
            template= self.template
        rendered= pystache.render(template, json.loads(jDic))
        return rendered
